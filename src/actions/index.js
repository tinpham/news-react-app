export const GET_NEWS = 'GET_NEWS'

export const getNews = (pageIndex, pageSize, searchValue) => ({
  type: GET_NEWS,
  pageIndex,
  pageSize,
  searchValue
})
