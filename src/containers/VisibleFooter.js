import { connect } from 'react-redux'
import Footer from '../components/Footer'

const mapStateToProps = state => ({
  links: state.links
})

export default connect(
  mapStateToProps
)(Footer)