import { connect } from 'react-redux'
import Header from '../components/Header'

const mapStateToProps = state => ({
  links: state.links
})

export default connect(
  mapStateToProps
)(Header)