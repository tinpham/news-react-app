import { connect } from 'react-redux'
import { getNews } from '../actions'
import NewsList from '../components/NewsList'

const mapStateToProps = state => ({
  items: state.news
})

const mapDispatchToProps = dispatch => ({
  getNews: (pageIndex, pageSize, searchValue) => dispatch(getNews(pageIndex, pageSize, searchValue))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewsList)