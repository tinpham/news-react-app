import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Container from '../components/Container/'

import Home from '../components/Home/'
import NewsProfile from '../components/NewsProfile/'
import Unavailable from '../components/Unavailable/'
import VisibleHeader from './VisibleHeader'
import VisibleFooter from './VisibleFooter'
import VisibleNewsList from './VisibleNewsList'

function Dashboard() {
  return (
    <Router>
      <VisibleHeader />
      <Container>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/news" component={VisibleNewsList} />
          <Route exact path="/news/:id" component={NewsProfile} />
          <Route component={Unavailable} />
        </Switch>
      </Container>
      <VisibleFooter />
    </Router>
  )
}

export default Dashboard