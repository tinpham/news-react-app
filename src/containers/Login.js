import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'

import Container from '../components/Container'
import LoginForm from '../components/LoginForm'

function Login() {
  return (
    <Router>
      <Container>
        <Route path="/" component={LoginForm} />
      </Container>
    </Router>
  )
}

export default Login