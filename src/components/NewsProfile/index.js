import React from 'react'
import PropTypes from 'prop-types'
import './index.css'

function NewsProfile({ match }) {
  return (
    <p>Displaying content of news #{match.params.id}</p>
  )
}

NewsProfile.propTypes = {
  match: PropTypes.object.isRequired
}

export default NewsProfile
