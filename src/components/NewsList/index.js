import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import './index.css'
import imgPlaceholder from '../../assets/images/image-placeholder.jpg'

function NewsList({items, match, getNews}) {

  const [ state, setState ] = useState({
    currentPageIndex: 0,
    pageSize: 15,
    searchValue: ''
  })

  const load = () => {
    getNews(state.currentPageIndex, state.pageSize, state.searchValue)
    setState({
      currentPageIndex: state.currentPageIndex + 1,
      pageSize: state.pageSize,
      searchValue: state.searchValue
    })
  }

  return (
    <div className="news-list">
      <div className="list">
        {items.map((item) =>
          <div className="list-item" key={item.id}>
            <Link to={`${match.url}/${item.id}`}>
              <h2 className="item-title">
                {item.title}
              </h2>
              <div className="item-cover">
                <img src={imgPlaceholder} alt=""/>
              </div>
            </Link>
            <div className="item-description">
              {item.description}
            </div>
            <div className="item-meta">
              <span className="item-meta-updated-date">
                Updated: {item.updated_date}
              </span>
            </div>
          </div>
        )}
      </div>
      {!items || items.length === 0
        ?
        <p className="list-empty" >
        No News
        </p>
        :
        <div className="list-footer">
          <button onClick={load}>
            Load More
          </button>
        </div>
      }
    </div>
  )
}


NewsList.propTypes = {
  items: PropTypes
    .arrayOf(
      PropTypes
        .shape({
          id: PropTypes.string.isRequired,
          title: PropTypes.string.isRequired,
          description: PropTypes.string.isRequired,
          updated_date: PropTypes.string.isRequired
        })
        .isRequired
    )
    .isRequired,
  match: PropTypes.object,
  getNews: PropTypes.func
}

export default NewsList
