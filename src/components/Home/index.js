import React from 'react'
import './index.css'

function Home() {
  return (
    <div className="home">
      <p>Welcome to Home of Good News !</p>
    </div>
  )
}

export default Home
