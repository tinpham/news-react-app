import React from 'react'
import './index.css'

function Unavailable() {
  return (
    <div className="unavailable">
      <p>The page you&apos;re looking for is unavailable !</p>
    </div>
  )
}

export default Unavailable
