import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'
import './index.css'

function Header({ links }) {

  const [state, setState] = useState({ caption: 'Home', navBoxHidden: true })

  return (
    <div className="header">
      <div className="top-bar">
        <div className="branding">
          <NavLink
            to="/"
            onClick={() => setState({ caption: 'Home', navBoxHidden: true })}>
            <div className="logo">Logo</div>
          </NavLink>
        </div>
        <h1 className="caption">{state.caption}</h1>
        <ul className={ state.navBoxHidden ? 'nav-box hidden' : 'nav-box' }>
          {links.map((link) =>
            <li key={link.caption}>
              <NavLink
                to={link.to}
                activeClassName="active"
                exact={link.exact}
                onClick={() => setState({ caption: `${link.caption}`, navBoxHidden: true })}>
                {link.caption}
              </NavLink>
            </li>
          )}
        </ul>
        <div className="nav-opener" onClick={() => setState({ caption: state.caption, navBoxHidden: false })}>
          <i className="fas fa-bars"></i>
        </div>
      </div>
      <div className="search-bar">
        <div className="search-box">
          <i className="fas fa-search"></i>
          <input type="text" placeholder="Search"/>
          <button>Go</button>
        </div>
      </div>
    </div>
  )
}

Header.propTypes = {
  links: PropTypes
    .arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        caption: PropTypes.string.isRequired,
        to: PropTypes.string.isRequired,
        exact: PropTypes.bool.isRequired
      })
    )
    .isRequired
}

export default Header
