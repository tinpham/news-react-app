import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import './index.css'

function Footer({ links }) {
  return (
    <div className="footer">
      <ul>
        {links.map((link) =>
          <li key={link.id}>
            <Link to={link.to}>{link.caption}</Link>
          </li>
        )}
      </ul>
      <span>Copyright © AMPOS</span>
    </div>
  )
}

Footer.propTypes = {
  links: PropTypes
    .arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        caption: PropTypes.string.isRequired,
        to: PropTypes.string.isRequired,
        exact: PropTypes.bool.isRequired
      })
    )
    .isRequired
}

export default Footer
