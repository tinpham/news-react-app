import React from 'react'
import ReactDOM from 'react-dom'
import App from './containers/App'

import './assets/styles/normalize.css'
import './assets/styles/icon.css'
import './assets/styles/style.css'

import * as serviceWorker from './serviceWorker'

import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from './reducers'
import thunk from 'redux-thunk'

const store = createStore(rootReducer, applyMiddleware(thunk))

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'))

serviceWorker.unregister()
