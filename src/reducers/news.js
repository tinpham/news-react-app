import { GET_NEWS } from '../actions'

function initNews() {
  let news = []
  for (let i = 0; i < 32; i++) {
    news.push({
      id: `${i}`,
      title: i % 2 ? 'What is Lorem Ipsum?' : 'Search me please!',
      description: `
        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
        when an unknown printer took a galley of type and scrambled it to make a type
        specimen book. It has survived not only five centuries, but also the leap
        into electronic typesetting, remaining essentially unchanged.`,
      cover: '/assets/images/image-placeholder.jpg',
      author: 'Tin Pham',
      updated_date: new Date().toDateString()
    })
  }
  return news
}

let items = initNews()

const news = (state = items, action) => {
  switch (action.type) {
  case GET_NEWS:
    return [
      ...state,
      {
        id: `${Date.now()}`,
        title: Date.now() % 2 ? 'What is Lorem Ipsum?' : 'Search me please!',
        description: `
            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
            when an unknown printer took a galley of type and scrambled it to make a type
            specimen book. It has survived not only five centuries, but also the leap
            into electronic typesetting, remaining essentially unchanged.`,
        cover: '/assets/images/image-placeholder.jpg',
        author: 'Tin Pham',
        updated_date: new Date().toDateString()
      }
    ]
  default:
    return state
  }
}

export default news
