const initialState = [
  { id: 'home', caption: 'Home', to: '/', exact: true },
  { id: 'news', caption: 'News', to: '/news', exact: false },
  { id: 'video', caption: 'Video', to: '/video', exact: false },
  { id: 'tv', caption: 'TV', to: '/tv', exact: false }
]

export default (state = initialState, action) => {
  switch (action.type) {
  default:
    return state
  }
}
