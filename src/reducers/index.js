import { combineReducers } from 'redux'
import links from './links'
import news from './news'

export default combineReducers({
  news,
  links
})