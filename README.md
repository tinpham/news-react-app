# News

This is porting of Angular news app into React.

## Start Development

- `npm start` to run the app in the development mode
- Open [http://localhost:3000](http://localhost:3000) to view it in the browser

The page will reload if you make edits. You will also see any lint errors in the console.

## Build Production

- `npm run build` to build the app for production to the `build` folder

It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.

Your app is ready to be deployed!

## Run Test

- `npm test` to launch the test runner in the interactive watch mode
